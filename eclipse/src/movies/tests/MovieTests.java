/**
 * @author Antonio
 * @author William
 *unless otherwise specified
 */
package movies.tests;
import movies.importer.Movie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MovieTests {
	Movie m=new Movie("1","2","3","4");

	@Test
	void testgetReleaseYear() {
		assertEquals("1",m.getReleaseYear());
	}
	@Test
	void testgetMovieName() {
		assertEquals("2",m.getMovieName());
	}
	@Test
	void testgetMovieRuntime() {
		assertEquals("3",m.getMovieRuntime());
	}
	@Test
	void testgetSource() {
		assertEquals("4",m.getSource());
	}

}
