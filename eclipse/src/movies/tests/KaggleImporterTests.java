/**
 * 
 * @author William
 *
 */
package movies.tests;
import movies.importer.KaggleImporter;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class KaggleImporterTests {
	
	@Test
	void testProcess() {
		ArrayList<String> test=new ArrayList<String>();
		test.add("1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20");
		test.add("1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20");
		test.add("1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20");
		KaggleImporter ki=new KaggleImporter("","");
		ArrayList<String> expected=new ArrayList<String>();
		expected.add("Release year: 20	Name: 16	Runtime: 14	Source: kaggle");
		expected.add("Release year: 20	Name: 16	Runtime: 14	Source: kaggle");
		expected.add("Release year: 20	Name: 16	Runtime: 14	Source: kaggle");
		
		assertEquals(expected.get(0),ki.process(test).get(0));
		assertEquals(expected.get(1),ki.process(test).get(1));
		assertEquals(expected.get(2),ki.process(test).get(2));
	}

}
