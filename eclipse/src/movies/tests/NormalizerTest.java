/**
 * @author William
 */
package movies.tests;
import movies.importer.Normalizer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class NormalizerTest {

	@Test
	void testProcess() {
		ArrayList<String> test=new ArrayList<String>();
		test.add("Release year: 1	Name: 2T	Runtime: 3 3	Source: kaggle");
		test.add("Release year: 4	Name: 5T	Runtime: 6 6	Source: kaggle");
		test.add("Release year: 7	Name: 8T	Runtime: 9 9	Source: kaggle");
		Normalizer n=new Normalizer("","");
		ArrayList<String> expected=new ArrayList<String>();
		expected.add("Release year: 1	Name: 2t 	Runtime: 3	Source: kaggle");
		expected.add("Release year: 4	Name: 5t 	Runtime: 6	Source: kaggle");
		expected.add("Release year: 7	Name: 8t 	Runtime: 9	Source: kaggle");
		
		assertEquals(expected.get(0),n.process(test).get(0));
		assertEquals(expected.get(1),n.process(test).get(1));
		assertEquals(expected.get(2),n.process(test).get(2));
	}

}
