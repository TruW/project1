package movies.importer;

import java.util.ArrayList;

/**
 * 
 * @author Antonio
 *
 */

public class ImdbImporter extends Processor {

	public ImdbImporter(String source, String destination) {
		super(source, destination, true);
	}

	public ArrayList<String> process(ArrayList<String> movieRaw) {
		ArrayList<String> movieList = new ArrayList<String>();
		for (int i = 0; i < movieRaw.size(); i++) {
			if (movieRaw.get(i).split("\\t").length == 22) {
				String[] des = movieRaw.get(i).split("\\t");
				Movie m = new Movie(des[3], des[1], des[6], "imdb");
				movieList.add(m.toString());
			}
		}
		return movieList;
	}
}
