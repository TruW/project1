/**
 * @author William
 * @author Antonio
 */
package movies.importer;

import java.util.ArrayList;

public class Deduper extends Processor{
	public Deduper(String sourceDir, String destinationDir) {
		super(sourceDir,destinationDir,false);
	}
	@Override
	public ArrayList<String> process(ArrayList<String> s){
		ArrayList<String> processed=new ArrayList<String>();
		processed.add(s.get(0));
		for(int i=1;i<s.size();i++) {
			boolean toAdd=true;
			for(int j=0;j<processed.size();j++) {
				if(s.get(i).split("\\t")[0].equals(processed.get(j).split("\\t")[0])&&s.get(i).split("\\t")[1].equals(processed.get(j).split("\\t")[1]))
					toAdd=false;
			}
			if(toAdd)
				processed.add(s.get(i));
		}
		return processed;
	}
//	@Override
//	public boolean equals(String s) {
//		boolean e=false;
//		if
//	}
//	idk how any of this would work or what it means lol 
}
