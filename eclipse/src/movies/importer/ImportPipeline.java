/**
 * @author William
 * @author Antonio
 */
package movies.importer;

import java.io.IOException;

public class ImportPipeline {

	public static void main(String[] args) throws IOException {
		String[] paths={"C:\\Users\\William\\Desktop\\Assignments\\prog III\\project1\\sourceK","C:\\Users\\William\\Desktop\\Assignments\\prog III\\project1\\sourceI","C:\\Users\\William\\Desktop\\Assignments\\prog III\\project1\\processorDestination","C:\\Users\\William\\Desktop\\Assignments\\prog III\\project1\\normalDestination","C:\\Users\\William\\Desktop\\Assignments\\prog III\\project1\\validDestination","C:\\Users\\William\\Desktop\\Assignments\\prog III\\project1\\dupeDestination"};
		processAll(paths);
	}
	public static void processAll(String[] p) throws IOException {
		KaggleImporter ki=new KaggleImporter(p[0],p[2]);
		ImdbImporter i=new ImdbImporter(p[1],p[2]);
		ki.execute();
		i.execute();
		Normalizer n=new Normalizer(p[2],p[3]);
		n.execute();
		Validator v=new Validator(p[3],p[4]);
		v.execute();
		Deduper d=new Deduper(p[4],p[5]);
		d.execute();
	}

}
