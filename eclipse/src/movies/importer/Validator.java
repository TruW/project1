package movies.importer;

import java.util.ArrayList;

/**
 * 
 * @author Antonio
 *
 */

public class Validator extends Processor {

	public Validator(String source, String destination) {
		super(source, destination, false);
	}

	public ArrayList<String> process(ArrayList<String> validate) {
		ArrayList<String> validater = new ArrayList<String>();
		for (int i = 0; i < validate.size(); i++) {
			String[] validaterSplit = validate.get(i).split("\\t");
			if (validaterSplit[0].equals("Release year: ")) {
				validate.remove(i);
			} else if (validaterSplit[1].equals("Name: ")) {
				validate.remove(i);
			} else if (validaterSplit[2].equals("Runtime: ")) {
				validate.remove(i);
			}

		}
		for (int j = 0; j < validate.size(); j++) {
			boolean toAdd = true;
			String[] parse = validate.get(j).split("\\t");
			String[] runtime = parse[2].split(" ");
			String[] release = parse[0].split(" ");
			try {
				Integer.parseInt(runtime[1]);
			} catch (Exception e) {
				System.out.println("runtime can't be converted into a number");
				toAdd = false;
			}
			try {
				Integer.parseInt(release[2]);
			} catch (Exception e) {
				System.out.println("release time can't be converted into a number");
				toAdd = false;
			}
			if (toAdd) {
				validater.add(validate.get(j));
			}
		}
		return validater;

	}
}
