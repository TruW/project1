package movies.importer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Antonio
 *
 */

class ValidatorTest {

	@Test
	void testProcess() {
		ArrayList<String> test = new ArrayList<String>();
		test.add("Release year: 2	Name: 	Runtime: 6	Source: imdb");
		test.add("Release year: 8	Name: 3	Runtime: word	Source: imdb");
		test.add("Release year: 1	Name: 4	Runtime: 5	Source: imdb");

		Validator validTest = new Validator("", "");
		ArrayList<String> answerTest = new ArrayList<String>();
		answerTest.add("Release year: 1	Name: 4	Runtime: 5	Source: imdb");
		assertEquals(answerTest.get(0), validTest.process(test).get(0));

	}

}
