package movies.importer;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author Antonio
 *
 */

class ImdbImporterTest {

	@Test
	void testProcess() {
		ArrayList<String> importer = new ArrayList<String>();
		importer.add("1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22");
		importer.add("1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22");
		importer.add("1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20	21	22");
		ImdbImporter testImport = new ImdbImporter("", "");
		ArrayList<String> answer = new ArrayList<String>();
		answer.add("Release year: 4	Name: 2	Runtime: 7	Source: imdb");
		answer.add("Release year: 4	Name: 2	Runtime: 7	Source: imdb");
		answer.add("Release year: 4	Name: 2	Runtime: 7	Source: imdb");
		assertEquals(answer.get(0), testImport.process(importer).get(0));
		assertEquals(answer.get(1), testImport.process(importer).get(1));
		assertEquals(answer.get(2), testImport.process(importer).get(2));
	}

}
