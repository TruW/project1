/**
 * 
 * @author William
 *
 */
package movies.importer;

import java.util.ArrayList;

public class Normalizer extends Processor{
	public Normalizer(String sourceDir, String destinationDir) {
		super(sourceDir,destinationDir,false);
	}
	@Override
	public ArrayList<String> process(ArrayList<String> s){
		ArrayList<String> processed=new ArrayList<String>();
		for(int i=0;i<s.size();i++) {
			String[] details=s.get(i).split("\\t");
			String[] title=details[1].split(" ");
			for(int j=1;j<title.length;j++)
				title[j]=title[j].toLowerCase();
			String nTitle="";
			for(int k=0;k<title.length;k++)
				nTitle+=title[k]+" ";
			String[] runtime=details[2].split(" ");
			String runtimeCut="	"+runtime[0]+" "+runtime[1];
			processed.add(details[0]+"	"+nTitle+runtimeCut+"	"+details[3]);
		}
		return processed;
	}
}
