/**
 * @author Antonio
 * @author William
 *unless otherwise specified
 */
package movies.importer;

public class Movie {
	private String releaseYear;
	private String movieName;
	private String movieRuntime;
	private String source;
	
	public Movie(String releaseYear, String movieName, String movieRuntime, String source) {
		this.releaseYear = releaseYear;
		this.movieName = movieName;
		this.movieRuntime = movieRuntime;
		this.source = source;
	}

	public String getReleaseYear() {
		return releaseYear;
	}

	public String getMovieName() {
		return movieName;
	}

	public String getMovieRuntime() {
		return movieRuntime;
	}

	public String getSource() {
		return source;
	}

	@Override
	public String toString() {
		return "Release year: " + releaseYear + "	Name: " + movieName + "	Runtime: " + movieRuntime
				+ "	Source: " + source;
	}
	
	
	
	

}
