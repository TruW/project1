/**
 * 
 * @author William
 *
 */

package movies.importer;
import java.util.ArrayList;

public class KaggleImporter extends Processor{
	public KaggleImporter(String sourceDir, String destinationDir) {
		super(sourceDir,destinationDir,true);
	}
	@Override
	public ArrayList<String> process(ArrayList<String> s){
		ArrayList<String> processed=new ArrayList<String>();
		for(int i=0;i<s.size();i++) {
			if(s.get(i).split("\\t").length==20) {
				String[] details=s.get(i).split("\\t");
				Movie m=new Movie(details[19],details[15],details[13],"kaggle");
				processed.add(m.toString());
			}
		}
		return processed;
	}
}
